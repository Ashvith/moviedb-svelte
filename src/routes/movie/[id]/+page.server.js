import { env } from '$env/dynamic/private';

export async function load({ params }) {
	const response = await fetch(
		`https://api.themoviedb.org/3/movie/${params.id}?api_key=${env.API_KEY}&language=en-US`
	);
	const movieDetail = await response.json();
	if (response.ok) {
		return {
			props: { movieDetail }
		};
	}
}
