import { env } from '$env/dynamic/private';

export async function load() {
	const response = await fetch(
		`https://api.themoviedb.org/3/movie/popular?api_key=${env.API_KEY}&language=en-US&page=1`
	);
	const data = await response.json();
	if (response.ok) {
		return {
			props: { popular: data.results }
		};
	}
}
